# Software Studio 2019 Spring Assignment 02

## Topic
* Project Name : Raiden

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle systems|10%|Y|
|Sound effects|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|Y|
|Appearance|5%|Y|

## Bonus
|Component|Score|Y/N|
|:-:|:-:|:-:|
|On-line Multi-player game|15%|N|
|Off-line Multi-player game|5%|N|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|
|Little helper|5%|N|
|Boss|5%|Y|

## Website Detail Description

# 作品網址：https://106062314.gitlab.io/Assignment_02

# Player Control : 
* Move: Arrow keys
* Low speed move: Shift + Arrow keys
* Fire: Z
* Ultimate skill: X
* BGM volume: U(up), I(down)
* SE volume: O(up), P(down)
* Pause: Esc
* (Cheat) Self explosion: C
* (Cheat) Clear all enemies: G


# Components Description : 
1. Complete game process<br>
遊戲有主選單→遊戲畫面→遊戲結束畫面，遊戲結束時可選擇回到主選單或再玩一次。
2. Basic rules<br>
    * 玩家：可由方向鍵控制，Z鍵可射擊，撞到敵人/敵方子彈會減少生命。
    * 敵人：系統會不斷生成敵人，每個敵人都可以移動和攻擊，撞到玩家子彈會減少生命。
    * 背景：會隨遊戲進行而捲動。
3. Jucify mechanisms<br>
    * 關卡：敵人的出現機制是固定的(第一波：兩排往外離去的敵人；第二波：隨機移動的敵人；第三波：高速下降的敵人；第四波：Boss)，擊敗Boss後關卡會重複，但敵人的生命值和發射子彈數會隨循環次數呈線性成長。
    * 技能：按下X鍵，玩家會向上方射出大量子彈，並進入短暫的無敵狀態(冷卻時間2秒)。
4. Animations<br>
玩家、敵人、玩家死亡、敵人中彈(未死亡)皆有動畫。
5. Particle systems<br>
敵人死亡時會噴射particles。
6. Sound effects<br>
    * 有背景音樂。
    * 玩家射擊、敵人射擊、玩家開大、玩家武器升級、玩家死亡、敵人死亡、敵人中彈(未死亡)皆有音效。
7. UI<br>
    * 畫面右側會依序顯示最高得分、得分、玩家生命殘量、玩家大招殘量、玩家能量(決定玩家武器強度)、玩家吃到的點數、音樂音量、音效音量。
    * 按下Esc可以暫停遊戲，暫停時按下Enter可以繼續遊戲。
8. Leaderboard<br>
利用Realtime database，在主選單顯示歷史最高分與該玩家名稱。
9. Bullet automatic aiming<br>
玩家power累積至8點以上時，會追加自瞄彈(藍色)，power愈高射擊頻率愈快。
10. Unique bullet<br>
玩家power累積至16點以上時，會追加雷射光。雷射光會造成持續穿透傷害。
11. Boss<br>
    * 有兩種攻擊模式：
        * 圓環狀的子彈。
        * 拋物線墜落的子彈。
    * 每隔一定時間隨機移動。

# Other Functions Description : 
1. 無敵時間<br>
玩家在遊戲開始時/剛死亡時/開大招時會有5秒/5秒/2秒的無敵時間，此時撞到敵人/敵方子彈不會觸發死亡事件(但還是可以吃到道具)。
2. 死亡懲罰<br>
玩家死亡時，power會減少16(最低歸0)，並散落power道具。
3. 低速模式<br>
壓住shift鍵時，玩家的移動速度會減慢，玩家子彈速度增快。
4. 判定點<br>
玩家有判定點(壓住shift鍵時方可看到)，敵人/敵方子彈觸及玩家判定點，才算死亡。
5. 外掛鍵<br>
    * 按下C鍵可以自爆。
    * 按下G鍵可以消滅場上所有敵人(除了Boss)。
6. 道具<br>
敵人死亡時有一定機率掉落道具。有紅、藍兩種顏色的道具。玩家吃到紅色道具會增加power(小的加1，大的加8)，吃到藍色道具時會增加大量得分(在愈高處吃到，得分愈高)。
7. 升級武器<br>
玩家必須累積power以升級武器(包含自瞄彈、雷射光)，升級點為8、16、32、48、64、80、128。

# Reference Game : 
東方紅魔鄉(https://www.youtube.com/watch?v=_OiUXqDWrFI)