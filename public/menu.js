var menuState = {
    create: function(){
        game.stage.backgroundColor = '#000000';
        game.add.image(0, 0, 'menu');

        var nameLabel = game.add.text(game.width/2, -80, 'Raiden', {font: '50px Arial', fill: '#ffffff'})
        nameLabel.anchor.setTo(0.5, 0.5);
        game.add.tween(nameLabel).to({y: 80}, 1200).easing(Phaser.Easing.Bounce.Out).start();

        var startLabel = game.add.text(game.width/2, 440, 'Press Enter to start', {font: '25px Arial', fill: '#ffffff'})
        startLabel.anchor.setTo(0.5, 0.5);

        var leaderboardKey = game.input.keyboard.addKey(Phaser.Keyboard.L);
        leaderboardKey.onDown.add(this.showLeaderboard, this);

        this.showLeaderboard();
        setTimeout(()=>{
            var startKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            startKey.onDown.add(this.start, this);
        }, 1000);
    },
    start: function(){
        game.state.start('play');
    },
    showLeaderboard: function(){
        var leaderboardRef = firebase.database().ref('leaderboard');
        leaderboardRef.once('value')
        .then(function(snapshot) {
            var obj = snapshot.val();
            var bestScore = 0;
            var bestPlayer;
            for(var key in obj) {
                if(obj[key].score > bestScore){
                    bestScore = obj[key].score;
                    if(obj[key].name) bestPlayer = obj[key].name;
                    else bestPlayer = 'Noname'
                }
            }
            this.bestScoreLabel = game.add.text(game.width/2, 400, 'Best score:   ' + bestPlayer + '   ' + bestScore, {font: '25px Arial', fill: '#ffffff'})
            this.bestScoreLabel.anchor.setTo(0.5, 0.5);
            game.global.highscoreDB = bestScore;
        })
        .catch(e => console.log(e.message));
    }
}